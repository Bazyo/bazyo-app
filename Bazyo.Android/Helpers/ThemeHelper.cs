﻿
using Bazyo.Enumeration;
using Bazyo.Themes;

namespace Bazyo.Droid.Helpers {
    public class ThemeHelper : IAppTheme {

        public void SetAppTheme(ThemeMode theme) { SetTheme(theme); }

        void SetTheme(ThemeMode theme) {
            if (theme == ThemeMode.DARK) {
                if (App.AppTheme == ThemeMode.DARK)
                    return;
                App.Current.Resources = new DarkTheme();
            } else if (theme == ThemeMode.LIGHT) {
                if (App.AppTheme == ThemeMode.LIGHT)
                    return;
                App.Current.Resources = new LightTheme();
            }
            App.AppTheme = theme;
        }
    }
}