﻿using Bazyo.Enumeration;

using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Bazyo.Settings {
    public static class Settings {

        public static ISettings AppSettings {
            get => CrossSettings.Current;
        }

        private const string IdTheme = "theme";
        public static int Theme {
            get => AppSettings.GetValueOrDefault(IdTheme, (int)ThemeMode.DARK);
            set {
                AppSettings.AddOrUpdateValue(IdTheme, value);
            }
        }
    }
}
