﻿using Bazyo.Enumeration;

namespace Bazyo.Themes {
    public interface IAppTheme {
        void SetAppTheme(ThemeMode theme);
    }
}
