﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Bazyo.Themes {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightTheme : ResourceDictionary {
        public LightTheme() {
            InitializeComponent();
        }
    }
}