﻿namespace Bazyo.Enumeration {
    public enum ThemeMode {
        LIGHT, DARK
    }
}
