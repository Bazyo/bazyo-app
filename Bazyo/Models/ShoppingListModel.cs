﻿using SQLite;

namespace Bazyo.Models {
    public class ShoppingListModel {
        [PrimaryKey, AutoIncrement]
        public int ShoppingId { get; set; }
        public string ArticleName { get; set; }
        public string Count { get; set; }
        public bool Done { get; set; }
    }
}
