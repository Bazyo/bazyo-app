﻿using Bazyo.Database;
using Bazyo.Models;

using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Bazyo.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShoppingItemList : ContentPage {
        public ShoppingItemList() {
            InitializeComponent();
        }

        protected override async void OnAppearing() {
            base.OnAppearing();
            ShoppingListDatabase database = await ShoppingListDatabase.Instance;
            ItemsList.ItemsSource = await database.GetItemsAsync();
        }

        async void AddNewItem(object sender, EventArgs e) {
            await Navigation.PushAsync(new ShoppingItem { BindingContext = new ShoppingListModel() });
        }

        private async void ItemsList_ItemSelected(object sender, SelectedItemChangedEventArgs e) {
            if (e.SelectedItem != null) {
                await Navigation.PushAsync(new ShoppingItem { BindingContext = e.SelectedItem as ShoppingListModel });
            }
        }
    }
}