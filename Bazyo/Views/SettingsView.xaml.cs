﻿using Bazyo.Enumeration;
using Bazyo.ViewModels;

using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Bazyo.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsView : ContentPage {
        public SettingsView() {
            InitializeComponent();
            ThemeModePicker.ItemsSource = Enum.GetValues(typeof(ThemeMode));
            BindingContext = new SettingsViewModel();
        }
    }
}