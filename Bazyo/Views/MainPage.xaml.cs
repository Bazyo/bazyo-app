﻿using Bazyo.ViewModels;

using Xamarin.Forms;

namespace Bazyo {
    public partial class MainPage : TabbedPage {
        public MainPage() {
            InitializeComponent();
            BindingContext = new MainPageViewModel();
        }
    }
}
