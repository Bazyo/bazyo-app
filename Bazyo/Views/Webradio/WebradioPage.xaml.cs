﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Bazyo.Views.Webradio {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WebradioPage : ContentPage {
        public WebradioPage() {
            InitializeComponent();
        }
    }
}