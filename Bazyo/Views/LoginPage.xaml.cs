﻿using Bazyo.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Bazyo.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage {
        public LoginPage() {
            InitializeComponent();
            BindingContext = new LoginViewModel();
        }
    }
}