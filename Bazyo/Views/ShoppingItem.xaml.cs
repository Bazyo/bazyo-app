﻿using Bazyo.Database;
using Bazyo.Models;

using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Bazyo.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShoppingItem : ContentPage {
        public ShoppingItem() {
            InitializeComponent();
        }

        async void OnSaveClicked(object sender, EventArgs e) {
            var item = (ShoppingListModel)BindingContext;
            ShoppingListDatabase database = await ShoppingListDatabase.Instance;
            await database.SaveItemAsync(item);
            await Navigation.PopAsync();
        }

        async void OnDeleteClicked(object sender, EventArgs e) {
            var item = (ShoppingListModel)BindingContext;
            ShoppingListDatabase database = await ShoppingListDatabase.Instance;
            await database.DeleteItemAsync(item);
            await Navigation.PopAsync();
        }

        async void OnCancelClicked(object sender, EventArgs e) {
            await Navigation.PopAsync();
        }
    }
}