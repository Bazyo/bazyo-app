﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bazyo.Services {
    internal interface IDataStore<T> {
        Task<bool> AddItemAsync(T item);
        Task<bool> DeleteItemAsync(string id);
        Task<bool> UpdateItemAsync(T item);
        Task<T> GetItemAsync(string id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}
