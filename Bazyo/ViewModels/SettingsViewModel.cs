﻿using Bazyo.Enumeration;

using GalaSoft.MvvmLight;

namespace Bazyo.ViewModels {
    public class SettingsViewModel : ViewModelBase {

        #region Theme
        public ThemeMode Theme {
            get => (ThemeMode)Settings.Settings.Theme;
            set {
                if (Settings.Settings.Theme != (int)value) {
                    Settings.Settings.Theme = (int)value;
                    RaisePropertyChanged(nameof(Theme));
                    App.AppTheme = value;
                }
            }
        }

        public void CheckTheme() {
            if (Theme == ThemeMode.DARK)
                App.AppTheme = ThemeMode.DARK;
            else if (Theme == ThemeMode.LIGHT)
                App.AppTheme = ThemeMode.LIGHT;
        }
        #endregion
    }
}
