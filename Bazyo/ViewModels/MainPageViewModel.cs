﻿using Bazyo.Views;
using Bazyo.Views.Webradio;

using GalaSoft.MvvmLight;

using System.Windows.Input;

using Xamarin.Forms;

namespace Bazyo.ViewModels {
    public class MainPageViewModel : ViewModelBase {

        public MainPageViewModel() {
            WelcomeText = "Herzlich willkommen";
            SettingsCommand = new Command(OnSettings);
            LogoutCommand = new Command(OnLogout);
            ShoppingListCommand = new Command(OnShoppingList);
            WebradioCommand = new Command(OnWebradio);
        }

        async void OnWebradio(object obj) => await App.Current.MainPage.Navigation.PushAsync(new WebradioPage());
        async void OnShoppingList(object obj) => await App.Current.MainPage.Navigation.PushAsync(new ShoppingItemList());
        async void OnSettings(object obj) => await App.Current.MainPage.Navigation.PushAsync(new SettingsView());
        async void OnLogout(object obj) {
            App.IsUserLoggedIn = false;
            await App.Current.MainPage.Navigation.PushAsync(new LoginPage());
        }

        public string WelcomeText { get; set; }

        public ICommand SettingsCommand { get; set; }
        public ICommand LogoutCommand { get; set; }
        public ICommand ShoppingListCommand { get; set; }
        public ICommand WebradioCommand { get; set; }
    }
}
