﻿using GalaSoft.MvvmLight;

using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace Bazyo.ViewModels {
    public class WebradioViewModel : ViewModelBase {

        public WebradioViewModel() {
            TrackBackwardCommand = new Command(OnBackwards);
            TrackForwardCommand = new Command(OnForwards);
            TrackPlayCommand = new Command(OnPlay);
            TrackStopCommand = new Command(OnStop);
        }

        private void OnStop(object obj) {
            
        }

        private void OnPlay(object obj) {
            
        }

        private void OnForwards(object obj) {
            
        }

        private void OnBackwards(object obj) {
            
        }

        public ICommand TrackBackwardCommand { get; private set; }
        public ICommand TrackForwardCommand { get; private set; }
        public ICommand TrackPlayCommand { get; private set; }
        public ICommand TrackStopCommand { get; private set; }
    }
}
