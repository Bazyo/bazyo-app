﻿using Bazyo.Views;

using GalaSoft.MvvmLight;

using System.Windows.Input;

using Xamarin.Forms;

namespace Bazyo.ViewModels {
    public class LoginViewModel : ViewModelBase {

        public LoginViewModel() {
            LoginCommand = new Command(OnLogin);
            SettingsCommand = new Command(OnSettings);
            Username = "";
            Password = "";
        }

        async void OnLogin(object obj) {
            if (Username.Equals("bazyo") && Password.Equals("bazyo")) {
                await App.Current.MainPage.Navigation.PushAsync(new MainPage());
            } else {
                await App.Current.MainPage.DisplayAlert("Login fehlgeschlagen!", "Benutzer oder Passwort sind falsch", "ok");
            }
        }

        async void OnSettings(object obj) => await App.Current.MainPage.Navigation.PushAsync(new SettingsView());

        public ICommand LoginCommand { get; set; }
        public ICommand SettingsCommand { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
