﻿using Bazyo.Models;

using SQLite;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bazyo.Database {
    public class ShoppingListDatabase {
        static SQLiteAsyncConnection Database;

        public static readonly AsyncLazy<ShoppingListDatabase> Instance = new AsyncLazy<ShoppingListDatabase>(async () => {
            var instance = new ShoppingListDatabase();
            CreateTableResult result = await Database.CreateTableAsync<ShoppingListModel>();
            return instance;
        });

        public ShoppingListDatabase() {
            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        }

        public Task<List<ShoppingListModel>> GetItemsAsync() {
            return Database.Table<ShoppingListModel>().ToListAsync();
        }

        public Task<List<ShoppingListModel>> GetItemsNotDoneAsync() {
            // SQL queries are also possible
            return Database.QueryAsync<ShoppingListModel>("SELECT * FROM [TodoItem] WHERE [Done] = 0");
        }

        public Task<ShoppingListModel> GetItemAsync(int id) {
            return Database.Table<ShoppingListModel>().Where(i => i.ShoppingId == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(ShoppingListModel item) {
            if (item.ShoppingId != 0) {
                return Database.UpdateAsync(item);
            } else {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(ShoppingListModel item) {
            return Database.DeleteAsync(item);
        }
    }
}
