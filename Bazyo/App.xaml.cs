﻿using Bazyo.Enumeration;
using Bazyo.Themes;
using Bazyo.Views;

using Xamarin.Forms;


namespace Bazyo {
    public partial class App : Application {

        public static ThemeMode AppTheme {
            get => Current.Resources is DarkTheme ? ThemeMode.DARK : ThemeMode.LIGHT;
            set {
                ResourceDictionary theme = null;
                switch (value) {
                    case ThemeMode.LIGHT:
                        theme = new LightTheme();
                        break;
                    case ThemeMode.DARK:
                        theme = new DarkTheme();
                        break;
                    default:
                        break;
                }

                var mergedDictionaries = Current.Resources.MergedDictionaries;
                if (mergedDictionaries != null && theme != null) {
                    mergedDictionaries.Add(theme);
                }
            }
        }

        public static bool IsUserLoggedIn { get; set; }

        public App() {
            InitializeComponent();

            if (!IsUserLoggedIn) {
                //MainPage = new NavigationPage(new LoginPage());
                MainPage = new NavigationPage(new LoginPage());
            } else {
                MainPage = new NavigationPage(new MainPage());
            }
        }

        protected override void OnStart() {
        }

        protected override void OnSleep() {
        }

        protected override void OnResume() {
        }
    }
}
